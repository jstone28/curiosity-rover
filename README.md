# curiosity-rover

A PoC Project for Postgres, Debezium, and Kafka

## Overview

Curiosity consumes messages from [Opportunity's](https://gitlab.com/jstone28/opportunity-rover) postgres via kafka by way of debezium-connect

![overview](./docs/overview.png)
