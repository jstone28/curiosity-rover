module gitlab.com/jstone28/curiosity-rover

go 1.13

require (
	github.com/Shopify/sarama v1.25.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/segmentio/kafka-go v0.3.5
)
