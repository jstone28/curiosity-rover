run:
	go run curiosity.go

build:
	go build curiosity.go

deploy:
	docker build -t registry.gitlab.com/jstone28/curiosity-rover .
	docker push registry.gitlab.com/jstone28/curiosity-rover
