FROM golang:1.13.5 as builder

# Configure Go
ENV GOPATH=/go \
    PATH=/go/bin:$PATH \
    CGO_ENABLED=0 \
    BASE_PATH=/ \
    TEST_ENV=localhost \
    GOOS=linux \
    GOARCH=amd64 \
    GO111MODULE=on \
	GOLANG_VERSION=1.13.5 \
    PATH=$GOPATH/bin:/usr/local/go/bin:$PATH

WORKDIR /

RUN apt-get update && apt-get install -y postgresql-client vim

COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY . .

RUN make build

CMD ["/curiosity"]
