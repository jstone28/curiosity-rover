package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Start starts the API server
func Start() {
	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "alive")
	})

	r.HandleFunc("/read", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "read")
	})

	log.Println("Listening on 3030")
	http.ListenAndServe(":3030", r)
}
