package kafka

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
)

// Reader reads from the kafka topic: opportunity
func Reader() {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{"kafka:9092"},
		Topic:     "postgres.public.opportunity",
		Partition: 0,
		MaxBytes:  10e6, // 10MB
	})
	defer r.Close()

	log.Println("Consumer started...")
	for {
		m, err := r.ReadMessage(context.Background())
		log.Println(err)
		log.Printf("Topic: %v", string(m.Topic))
		log.Printf("Message: %v", string(m.Value))
	}
}

// Consumer is the alternative to reader
func Consumer() {
	// to consume messages
	topic := "postgres.public.opportunity"
	partition := 0
	conn, err := kafka.DialLeader(context.Background(), "tcp", "kafka:9092", topic, partition)
	log.Println(err)

	conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	batch := conn.ReadBatch(10e3, 1e6) // fetch 10KB min, 1MB max

	b := make([]byte, 10e6) // 10KB max per message
	log.Println("Consumer started...")
	for {
		_, err := batch.Read(b)
		if err != nil {
			break
		}
		fmt.Println(string(b))
	}

	batch.Close()
	conn.Close()
}
